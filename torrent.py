# coding=UTF-8

import sys, os

class Torrent(object):
	def __init__(self, dataPath):
		if self.__class__.__name__ == "Torrent":
			raise Exception, "You should not try to instance this class directly"			
		
		self.dataPath = dataPath
				
		(_, self.name) = os.path.split(self.dataPath)
		(self.name, _) = os.path.splitext(self.name)
	
	def link(self):
		raise Exception, "This is method of abstract class and should be overrided"
	


class SingleTorrent(Torrent):
	
	def __init__(self, dataPath, filePath=None):
		super(SingleTorrent, self).__init__(dataPath)
		if filePath:
			self.dataPath = filePath
		(_, self.ext) = os.path.splitext(self.dataPath)
	
	def link(self, dest):
		dest = os.path.join(dest, self.name) + self.ext
		os.link(self.dataPath, dest)

class DiskTorrent(Torrent):
	
	def __init__(self, dataPath, diskRoot):
		super(DiskTorrent, self).__init__(dataPath)
		self.diskRoot = diskRoot
	
	def link(self, dest):
		newRoot = os.path.join(dest, self.name)
		os.mkdir(newRoot)	
		for (root, dirs, files) in os.walk(self.diskRoot):
			if root[-12:]==".AppleDouble": continue #to exclude .AppleDouble on NAS
			for d in dirs:
				if d==".AppleDouble": continue #to exclude .AppleDouble on NAS
				relPath = os.path.relpath(os.path.join(root,d), self.diskRoot)
				os.mkdir(os.path.join(newRoot, relPath))
			for f in files:
				relPath = os.path.relpath(os.path.join(root,f), self.diskRoot)
				os.link(os.path.join(self.diskRoot, relPath), os.path.join(newRoot, relPath))		


class MultiTorrent(Torrent):
	
	def __init__(self, dataPath, files):
		super(MultiTorrent, self).__init__(dataPath)
		self.files = files
		
	def link(self, dest):
		d = os.path.join(dest, self.name)
		os.mkdir(d)
		for f in self.files:
			(_, fileName) = os.path.split(f)
			os.link(f, os.path.join(d, fileName))

def getTorrent(dataPath, extentios, diskMarkerFiles):
	if not os.path.exists(dataPath):
		raise IOError, u"Data path \"{0}\" specified for the torrent is incorrect".format(dataPath)
		
	if os.path.isfile(dataPath): 
			(_,ext) = os.path.splitext(dataPath)
			if ext[1:].upper() in extentios:
				return SingleTorrent(dataPath) #Torrent is single file
			else:
				raise Exception, u"{0} file is not of watched type".format(dataPath)
	else: #Torrent is a directory
		foundFiles = []
		for (root, dirs, files) in os.walk(dataPath):
			if root[-12:]==".AppleDouble": continue #to exclude .AppleDouble on NAS
			if set(diskMarkerFiles) & set([f.upper() for f in files]):
				return DiskTorrent(dataPath, root) #Torrent is a directory with copy of laser disc	

			for f in files:
				(_,ext) = os.path.splitext(f)
				if ext[1:].upper() in extentios: #found file
					foundFiles.append(os.path.join(root,f))

		if len(foundFiles)==1: # At this point no disk directories found
			return SingleTorrent(dataPath, foundFiles[0])

		if len(foundFiles)>1:
			return MultiTorrent(dataPath, foundFiles)

	raise Exception, u"No video content found in {0}".format(dataPath)
