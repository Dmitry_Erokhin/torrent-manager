#!/usr/bin/env python
# coding=UTF-8

import os, sys, argparse
from sys import getfilesystemencoding as enc
import logging as l
import settings, torrent

def createPath(newPath):
	if not os.path.exists(newPath):
		subDirs = []
		tempPath = newPath
		while True:
			(tempPath, tail) = os.path.split(tempPath)
			if not tail: break
			subDirs.append(tail)		
		subDirs.reverse()		
		for d in subDirs:
			tempPath = os.path.join(tempPath, d)
			if not os.path.exists(tempPath): os.mkdir(tempPath)
	
def patchLink():
	def win_link(source, destination):
		cmd = u"fsutil hardlink create \"{0}\" \"{1}\"".format(destination, source)
		exitCode = os.system(cmd.encode(enc()))	
		if exitCode: raise IOError, u"Cannot create hard link {0} pointing to {1}".format(destination,source)
	os.link = win_link

def main():
	l.info("Programm started")

	if sys.platform[:3] == "win": patchLink()

	ap = argparse.ArgumentParser(description="Create hardlinks to video torrents in specified root dir", 
									epilog='Use python torrentman.py "K" "D" "F" in utorrent "run on finish"')
	ap.add_argument('type', choices=["single", "multi"], help="Type of torrent: \"single\" for single file, \"multi\" for directory (K in utorrent)")
	ap.add_argument('torrent_root', help="Directory, where torrent was downloaded (D in utorrent)")
	ap.add_argument('torrent_file', help="Torrent file name, should be scpecified, but make sense only in case of 'single' type (F in utorrent)")
#	ap.add_argument('-s','--settings', help="Optional settings file")
	args = ap.parse_args()

	args.torrent_root = args.torrent_root.decode(enc())
	args.torrent_file = args.torrent_file.decode(enc())
#	if args.settings: args.settings = args.settings.decode("windows-1251")
	
	l.debug(u"Got argummens: {0}".format(args))
	
	if args.type=="single":
		torrentPath = os.path.join(args.torrent_root, args.torrent_file)
	else:
		torrentPath = args.torrent_root

	if not torrentPath.startswith(settings.INPUT_ROOT):
		l.critical(u"Can not process torrent {0} cause it's not in {1}".format(torrentPath, settings.INPUT_ROOT))
		exit(1)
	
	try:
		t = torrent.getTorrent(torrentPath, settings.EXTENTIONS, settings.DISK_MARKERS)
	except Exception as err:
		l.critical(u"Can not initialize torrent - {0}".format(err.message))
		exit(1)

	(linkDir,_) = os.path.split(torrentPath)
	relPath = os.path.relpath(linkDir, settings.INPUT_ROOT)
	newPath = os.path.join(settings.OUTPUT_ROOT, relPath)

	try:
		createPath(newPath)
	except Exception as err:
		l.critical(u"Can not create destination path - {0}".format(err.message))
		exit(1)

	try:
		t.link(newPath)
	except Exception as err:
		l.critical(u"Can not make links - {0}".format(err.message))
		exit(1)

	
	if hasattr(settings, "XBMC_UPDATE") and list((x for x in settings.XBMC_UPDATE if newPath.startswith(x))): #if we need to update library
		from xbmcbridge import XBMC
		x = XBMC(host=settings.XBMC_HOST, port=settings.XBMC_PORT, name=settings.XBMC_LOGIN, pwd=settings.XBMC_PASSWORD)
		x.notify("Download complete", t.name)
		x.update(newPath)

 	l.info("Programm finished normaly")

if __name__ == "__main__":
	try:
		l.basicConfig(filename=settings.LOG_FILE, format='[%(asctime)s] %(levelname)s: %(message)s', level=settings.LOG_LEVEL)
	except IOError:
		l.basicConfig(filename=settings.LOG_FILE, filemode="w", format='[%(asctime)s] %(levelname)s: %(message)s', level=settings.LOG_LEVEL)
	main()