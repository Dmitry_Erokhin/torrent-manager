# coding=UTF-8

import urllib
#from sys import getfilesystemencoding as enc

class XBMC (object):
	def __init__(self, host, port, name, pwd):
		self.base = "http://{0}:{1}@{2}:{3}".format(name, pwd, host, port)
	
	def __execBuiltin(self, command):
		if type(command) == unicode: command = command.encode("utf-8")
		cmd = self.base + "/xbmcCmds/xbmcHttp?command=ExecBuiltIn(" + urllib.quote(command) + ")"
		try:
			urllib.urlopen(cmd)
		except:
			return False
		else:
			return True
	
	def notify(self, head, text): return self.__execBuiltin(u"Notification(\"{0}\",\"{1}\")".format(head, text))

	def update(self, path, tp="video"): return self.__execBuiltin(u"UpdateLibrary({0},\"{1}\")".format(tp, path))